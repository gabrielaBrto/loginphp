<?php

    ob_start();
    require("../conexao/conexao.php");

    $email = $_POST['email'];
    $password = sha1($_POST['password']);

    $stmt = $con->prepare("SELECT * from clientes WHERE email = :email and password = :password"); 
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);
    $stmt->execute();

    if($stmt->rowCount() == 1){

            session_start();
            $_SESSION['email'] = $email;
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $_SESSION['id'] = $result['id'];
    
        header("location: ../logado.php");
        
    }else{
      header("Location: ../login.php?error=Algo deu errado");
    }


?>