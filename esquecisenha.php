<?php include 'templates/header.php'?>
<div class="container">
    <?php
        if($_GET){
        if($mensagem = $_GET['sucesso']){
    ?>   
        <div class="alert alert-success mt-2" role="alert">
          <?php echo $mensagem?>
        </div>     
    <?php    
        }elseif($mensagem = $_GET['error']){
    ?>  
        <div class="alert alert-danger mt-2" role="alert">
          <?php echo $mensagem?>
        </div>
    <?php 
        }  
    }
    ?>
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
        <div class="alert alert-warning" role="alert">
          Um link será enviado para o seu e-mail
        </div>
        <div class="card">
            <div class="card-header text-center">
                ESQUECI MINHA SENHA
            </div>
            <form method ="POST" action="src/recuperarsenha.php">
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted text-center">
                    <button class="btn btn-success">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include 'templates/footer.php'?>