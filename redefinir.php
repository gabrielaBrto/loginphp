<?php include 'templates/header.php'?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
        <div class="alert alert-info" role="alert">
          Entre com a sua nova senha:
        </div>
        <div class="card">
            <div class="card-header text-center">
                REDEFINIÇÃO DE SENHA
            </div>
            <form action="src/alterarsenha.php">
                <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Senha</label>
                        <input type="password" class="form-control" name="password" placeholder="Senha">
                    </div>
                </div>
                </div>
                <input type="hidden" name="token" value="<?php echo $_GET['token'] ?>">
                <input type="hidden" name="email" value="<?php echo $_GET['email']?>">
                <div class="card-footer text-muted text-center">
                    <button class="btn btn-success">Enviar</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<?php include 'templates/footer.php'?>