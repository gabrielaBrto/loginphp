
<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cliente</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1">CLIENTES</span>
  <?php 
	if((!isset ($_SESSION['email']) == true) and (!isset ($_SESSION['password']) == true)){
  		unset($_SESSION['email']);
  		unset($_SESSION['password']);
  	}
 	if($_SESSION){
  		echo '<a href="logout.php">Sair</a>';
  	}
  ?>
  </nav>