<?php include 'templates/header.php'?>
    <?php
    require("conexao/conexao.php");
        if($_GET){
        if($mensagem = $_GET['sucesso']){
    ?>   
        <div class="alert alert-success mt-2" role="alert">
          <?php echo $mensagem?>
        </div>     
    <?php    
        }elseif($mensagem = $_GET['error']){
    ?>  
        <div class="alert alert-danger mt-2" role="alert">
          <?php echo $mensagem?>
        </div>
    <?php 
        }  
    }
    ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-5">
        	<div class="alert alert-info" role="alert">
			  <h4 class="alert-heading">Login realizado com sucesso</h4>
			  <hr>
			  <p class="mb-0">Bem Vindo <?php echo $_SESSION['email'];?> !</p>
			</div>
        </div>
    </div>
</div>
<?php include 'templates/footer.php'?>