-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema loginphp
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema loginphp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `loginphp` DEFAULT CHARACTER SET latin1 ;
USE `loginphp` ;

-- -----------------------------------------------------
-- Table `loginphp`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loginphp`.`clientes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `token` VARCHAR(60) NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `loginphp`.`historico_senha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `loginphp`.`historico_senha` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `clientes_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_historico_senha_clientes_idx` (`clientes_id` ASC) VISIBLE,
  CONSTRAINT `fk_historico_senha_clientes`
    FOREIGN KEY (`clientes_id`)
    REFERENCES `loginphp`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
