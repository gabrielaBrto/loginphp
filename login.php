<?php include 'templates/header.php'?>
<div class="container">
    <?php
        if($_GET){
        if($mensagem = $_GET['sucesso']){
    ?>   
        <div class="alert alert-success mt-2" role="alert">
          <?php echo $mensagem?>
        </div>     
    <?php    
        }elseif($mensagem = $_GET['error']){
    ?>  
        <div class="alert alert-danger mt-2" role="alert">
          <?php echo $mensagem?>
        </div>
    <?php 
        }  
    }
    ?>
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
        <div class="card">
            <div class="card-header text-center">
                LOGIN
            </div>
            <form action="src/login.php" method="POST">
                <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Senha</label>
                        <input type="password" class="form-control" name="password" placeholder="Senha">
                    </div>
                </div>
                </div>
                <div class="card-footer text-muted text-center">
                    <button class="btn btn-success">Enviar</button>
                    <div class="mt-2">
                        <a href="esquecisenha.php">Esqueci Minha Senha</a>   
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<?php include 'templates/footer.php'?>